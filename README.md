# Backend App API proxy

NGINX proxy app for our backend app API

## Usage

### Env Vars

* `LISTEN_PORT` - Port to listen on (`8000`)
* `APP_HOST` - Hostname of the app to forward requests to (`app`)
* `APP_PORT` - Port of the app to forwad request to (`9000`)